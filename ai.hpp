#ifndef AI_HPP
#define AI_HPP

#include "fighter.hpp"
#include "platform.hpp"
#include "SDL2/SDL.h"

using namespace std;

class AI: public Fighter {
public:
	AI(SDL_Renderer *screen, int screenW, int screenH, int startX, int startY, string path)
		:Fighter(screen, screenW, screenH, startX, startY, path) {
			health = 100.0;
			type = "AI";
			create(startX, startY, path);
	}
	

};

#endif