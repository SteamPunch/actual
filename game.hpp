#include <iostream>
#include <string>
#include <vector>

#include "SDL2/SDL.h"
#include "sprite.hpp"
#include "player.hpp"
#include "fighter.hpp"
#include "ai.hpp"
#include "platform.hpp"
#include "Hud.hpp"
#include "renderable.hpp"
#include "util.hpp"
#include "mediaManager.hpp"

using namespace std;

class Game {
public:
	Game(){}

	void init() {
		window = NULL;
		renderer = NULL;
		int posX = 100, posY = 100, width = 640, height = 480;


		SDL_Init(SDL_INIT_VIDEO);
		window = SDL_CreateWindow("SteamPunch!", posX, posY, width, height, 0);
		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);

		mm->loadSpriteSheets(renderer, "spriteWalk.txt");
		mm->loadSpriteSheets(renderer, "spriteOther.txt");

		player = new Player(renderer, width, height, 320, 240, "hello.bmp");
		renderables.push_back(player);
		sprites.push_back(player);

		ai = new AI(renderer, width, height, 200, 240, "hello.bmp");
		renderables.push_back(ai);
		sprites.push_back(ai);

		platform = new Platform(renderer, width, height, 60, 350, "hello.bmp");
		renderables.push_back(platform);
		sprites.push_back(platform);

		hb1 = new HealthBar(renderer, player);
		renderables.push_back(hb1);

		last = SDL_GetTicks();
	}

	void go() {
		while(1){
			float dT = float(SDL_GetTicks() - last)/1000.0;

			if(SDL_PollEvent(&event)){
				if(event.type == SDL_KEYUP || event.type == SDL_KEYDOWN){
					player->userInput(&event, player);//I added a paramter to pass the other player so the other player's health would go down
				}
				else if(event.type == SDL_QUIT)
					break;
			}


			player->update(dT, platform);
			hb1->update();
			ai->update(dT, platform);

			if (player->getHealth() == 0) {
				break;
			}

			SDL_RenderClear(renderer);
			for (Uint32 i = 0; i < renderables.size(); ++i) {
				renderables[i]->draw();
			}
			last = SDL_GetTicks();
			SDL_RenderPresent(renderer);
		}

		SDL_DestroyRenderer(renderer);
		SDL_DestroyWindow(window);
	}

protected:
	SDL_Window *window;
	SDL_Renderer *renderer;
	vector<Renderable *> renderables;
	vector<Sprite *> sprites;
	Player *player;
	AI *ai;
	HealthBar *hb1;
	Platform *platform;
private:
	SDL_Event event;
	int last;
};

extern MediaManager *mm;
