#ifndef PLATFORM_HPP
#define PLATFORM_HPP

#include "sprite.hpp"
#include "SDL2/SDL.h"

using namespace std;

class Platform: public Sprite {
public:
	Platform(SDL_Renderer *screen, int screenW, int screenH, int startX, int startY, string path)
		:Sprite(screen, screenW, screenH, startX, startY, path) {
			create(startX, startY, path);
			type = "Platform";
		}

	void create(int startX, int startY, string path) {
		Sprite::create(startX, startY, path);
		spriteRect.w = 500;
		spriteRect.h = 30;
	}

	void getPlatformInfo(int *returnedX, int *returnedY, int *returnedW, int *returnedH) {
		*returnedX = this->x;
		*returnedY = this->y;
		*returnedW = this->spriteRect.w;
		*returnedH = this->spriteRect.h;
	}

	void update(){
	}
};

#endif
