#ifndef FIGHTER_HPP
#define FIGHTER_HPP

#include "sprite.hpp"
#include "platform.hpp"
#include "SDL2/SDL.h"
#include "util.hpp"
#include "mediaManager.hpp"

using namespace std;

class Fighter: public Sprite {
public:
	Fighter(SDL_Renderer *screen, int screenW, int screenH, int startX, int startY, string path)
		:Sprite(screen, screenW, screenH, startX, startY, path) {
			health = 100.0;
			type = "Fighter";
			create(startX, startY, path);
		}

	virtual void create(int startX, int startY, string path) {
		Sprite::create(startX, startY, path);
		ay = 95;
		spriteRect.w = 50;
		spriteRect.h = 100;
		counter = 0;
		last = State::IDLE;
	}

	SDL_Rect* loadRect(SpriteSequence current, int last) {
		SDL_Rect* temp = new SDL_Rect();
		int start = (last + 1) * 4;
		temp->x = current.coords[start];
		temp->y = current.coords[start + 1];
		temp->w = current.coords[start + 2];
		temp->h = current.coords[start + 3];
		return temp;
	}

	void draw() {
		SDL_Surface *nextSurface;
		SDL_Rect *tempRect;
		SDL_Texture *nextTexture;
		spriteRect.x = (int)x;
		spriteRect.y = (int)y;
		if(current == State::IDLE && last != State::IDLE){
			++counter;
			tempRect = loadRect(mm->getSpriteSequence(State::IDLE), counter);
			SDL_BlitSurface(mm->surfaces[State::IDLE], tempRect, nextSurface, &spriteRect);
			last = State::IDLE;
		}
		spriteTexture = SDL_CreateTextureFromSurface(screen, nextSurface);
		SDL_RenderCopy(screen, nextTexture, NULL, &spriteRect);
	}

	void update(float dT, Platform *platform) {
		vx += ax * dT;
		vy += ay * dT;
		x += vx * dT;
		y += vy * dT;

		if(collidesWith(platform)){
			//SDL_Log("platform");
			vy = 0;
			ay = 0;
		}
		else
			ay = 95;

		if(x > screenW - spriteRect.w || x < 0)
			vx = -1*vx;
		if(y > screenH - spriteRect.h || y < 0){
			health = 0;
		}
	}

	int getHealth() {
		return (int)health;
	}
	

protected:
	float health;

private:
	State current, last;
	int counter;
};

extern MediaManager *mm;

#endif
