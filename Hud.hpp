#ifndef HUD_HPP
#define HUD_HPP

#include "SDL2/SDL.h"
#include "sprite.hpp"
#include "player.hpp"
#include "renderable.hpp"

class HealthBar : public Renderable{
private:
	Player *s;

	protected:
	float x;
	//float y;
	float healthw;
	int currenthealth;
	SDL_Texture *bitmapTex;
	SDL_Rect health;
	SDL_Renderer *renderer;
	
	public:
	HealthBar(SDL_Renderer *_renderer, Player *_s){//, SDL_Rect health){
		SDL_Surface *bitmapSurface;
		renderer = _renderer;
		bitmapSurface = SDL_LoadBMP("hello.bmp");
		bitmapTex = SDL_CreateTextureFromSurface(renderer, bitmapSurface);
		currenthealth=100;
		health.h=50;
		health.w=200;
		s = _s;
		SDL_FreeSurface(bitmapSurface);
	}
	
	void update() {		
		//SDL_Surface *bitmapSurface;
		currenthealth = ((s->getHealth() * 2));// * .01) * 467);
		health.w = currenthealth;
		//SDL_FillRect(bitmapSurface, health, SDL_MapRGB(bitmapSurface->format, 0, 255, 0));
		//bitmapTex = SDL_CreateTextureFromSurface(renderer, bitmapSurface);
	}
	
	void draw(){//, SDL_Rect dest){
		SDL_RenderCopy(renderer, bitmapTex, NULL, &health);
	}
	~HealthBar(){
		SDL_DestroyTexture(bitmapTex);
	}
};

#endif
