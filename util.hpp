#ifndef UTIL_HPP
#define UTIL_HPP

#include <string>
#include <vector>
#include "SDL2/SDL.h"

using namespace std;

enum class State {
	WALKING,
	PUNCH,
	IDLE
};

struct SpriteSequence {
	State which;
	int numBlits;
	vector<int> coords;
};

#endif