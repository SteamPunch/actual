#ifndef PLAYER_HPP
#define PLAYER_HPP 

#include "fighter.hpp"
#include "platform.hpp"
#include "SDL2/SDL.h"

using namespace std;

class Player: public Fighter {
public:
	Player(SDL_Renderer *screen, int screenW, int screenH, int startX, int startY, string path)
		:Fighter(screen, screenW, screenH, startX, startY, path) {
			this->health = 100.0;
			type = "Player";
			create(startX, startY, path);
	}

	void userInput(SDL_Event *event, Fighter *otherFighter){
		if(event->type == SDL_KEYDOWN){
			switch(event->key.keysym.sym){
				case SDLK_LEFT:
					vx = -45;

					break;
				case SDLK_RIGHT:
					vx = 45;
					break;
				case SDLK_SPACE:
					vy = -80;
					break;
				case SDLK_s:
					//health -=10;
					//punch(otherFighter);
					break;
				default:
					break;
			}
		}
		else if (event->type == SDL_KEYUP){
			SDL_Log("KeyUp");
			switch(event->key.keysym.sym) {
				case SDLK_LEFT:
					vx = 0;
					break;
				case SDLK_RIGHT:
					vx = 0;
					break;
				case SDLK_s:
					//release();//On key release, the texture becomes the original surface again and is drawn
					break;
				default:
					break;
			}
		}
	}
};

#endif