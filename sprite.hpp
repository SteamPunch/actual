#ifndef SPRITE_HPP
#define SPRITE_HPP

#include <sstream>
#include "SDL2/SDL.h"
#include "renderable.hpp"
#include "util.hpp"

using namespace std;

class Sprite : public Renderable {
	public:
		Sprite(SDL_Renderer *_screen, int _screenW, int _screenH, int startX, int startY, string path) {
			screen = _screen;
			screenW = _screenW;
			screenH = _screenH;
			create(startX, startY, path);
		}

		virtual void create(int startX, int startY, string path) {
			SDL_Surface *bitmapSurface;

			ax = 0;
			ay = 0;
			vx = 0;
			vy = 0;
			x = startX;
			y = startY;

			bitmapSurface = SDL_LoadBMP(path.c_str());
			spriteTexture = SDL_CreateTextureFromSurface(screen, bitmapSurface);
			SDL_FreeSurface(bitmapSurface); 

			spriteRect.x = x;
			spriteRect.y = y;
			spriteRect.w = bitmapSurface -> w;
			spriteRect.h = bitmapSurface -> h;
		}
		
		virtual void createNextTexture(int currentX, int currentY, string path)
		{
			SDL_Surface *bitmapSurface;
			
			bitmapSurface = SDL_LoadBMP(path.c_str());
			spriteTexture = SDL_CreateTextureFromSurface(screen, bitmapSurface);
			SDL_FreeSurface(bitmapSurface);
			
			spriteRect.x = currentX;
			spriteRect.y = currentY;
			spriteRect.w = bitmapSurface -> w;
			spriteRect.h = bitmapSurface -> h;
			
		}

		virtual void draw() {
			spriteRect.x = (int)x;
			spriteRect.y = (int)y;
			SDL_RenderCopy(screen, spriteTexture, NULL, &spriteRect);
		}

		bool contains(int otherX, int otherY) {
			//SDL_Log("contains called, %i %i : %i %i", otherX, otherY, spriteRect.x, spriteRect.y);
			bool contains = (spriteRect.x <= otherX && otherX <= spriteRect.x + spriteRect.w) ||
				(spriteRect.y <= otherY && otherY <= spriteRect.y + spriteRect.h);
			//SDL_Log("%d", contains);
			return contains;
		}

		bool collidesWith(Sprite *other) {
			//SDL_Log("collidesWith called");
			return contains(other->spriteRect.x, other->spriteRect.y) ||
				contains(other->spriteRect.x + other->spriteRect.w, other->spriteRect.y) ||
				contains(other->spriteRect.x, other->spriteRect.y + other->spriteRect.h) ||
				contains(other->spriteRect.x + other->spriteRect.w, other->spriteRect.y + other->spriteRect.h);
		}

		/* For future use? 
		vector<Sprite *> collidesWith(vector<Sprite *> allSprites){
			vector<Sprite *> collisions;
			for(uint i = 0; i < allSprites.size(); i++) {
				if(this.collidesWith(allSprites[i]))
					collisions.push_back(allSprites[i]);
			}
		} */

	protected:
		SDL_Renderer *screen;
		SDL_Texture *spriteTexture;
		SDL_Rect spriteRect;
		float ax; //x-axis acceleration
		float ay; //y-axis acceleration
		float vx; //x-axis velocity
		float vy; //y-axis velocity
		float x; //x coord placeholder
		float y; //y coord placeholder
		float t;
		int totalTime;
		int screenW;
		int screenH;
		string type;
	private:
};

#endif
