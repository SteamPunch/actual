#ifndef MEDIAMANAGER_HPP
#define MEDIAMANAGER_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include "SDL2/SDL.h"
#include "util.hpp"

using namespace std;

class MediaManager {
public:
	map<State, SpriteSequence> sprites;
	map<State, SDL_Surface*> surfaces;

	void loadSpriteSheets(SDL_Renderer *renderer, string file) {
		int numBlits, numStates;
		SDL_Surface *tempSurface;
		SpriteSequence tempSpriteSequence;
		State tempState;
		string imageFileName, state;

		ifstream inf;
		inf.open(file);
		inf >> imageFileName >> numStates;

		if(!imageFileName.empty()){
			for (int i = 0; i < numStates; ++i) {
				tempSurface = SDL_LoadBMP(imageFileName.c_str());

				inf >> state;
				tempState = stringToState(state);

				inf >> numBlits;

				tempSpriteSequence.which = tempState;
				tempSpriteSequence.numBlits = numBlits;
				for (int i = 0; i < numBlits * 4; ++i)
				{
					int coord;
					inf >> coord;
					tempSpriteSequence.coords.push_back(coord);
				}
			sprites[tempState] = tempSpriteSequence;
			surfaces[tempState] = tempSurface;
			}
		}
		else
			SDL_Log("Error! Missing input file for sprite sheet!\n");
	}

	SpriteSequence getSpriteSequence(State which) {
		return sprites[which];
	}

protected:

private:
	State stringToState(string temp) {
		if (temp == "WALKING")
			return State::WALKING;
		else if (temp == "PUNCH")
			return State::PUNCH;
		else if (temp == "IDLE")
			return State::IDLE;
		else {
			SDL_Log("Unknown state [%s] passed! Returning IDLE to prevent crash!\n", temp.c_str());
			return State::IDLE;
		}
	}
	
};

extern MediaManager *mm;

#endif